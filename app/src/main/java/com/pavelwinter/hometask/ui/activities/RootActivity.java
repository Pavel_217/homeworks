package com.pavelwinter.hometask.ui.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pavelwinter.hometask.BuildConfig;
import com.pavelwinter.hometask.R;
import com.pavelwinter.hometask.mvp.presenters.AuthPresenter;
import com.pavelwinter.hometask.mvp.views.IAuthPresenter;
import com.pavelwinter.hometask.mvp.views.IAuthView;
import com.pavelwinter.hometask.ui.custom_views.AuthPanel;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RootActivity extends Activity implements IAuthView,View.OnClickListener{

AuthPresenter mPresenter=AuthPresenter.getInstance();


    String loginKey="loginKey";
    String passwordKey="passwordKey";


    public String fontNeueBook="fonts/PTBebasNeueBook.ttf";
    public String fontNeueRegular="fonts/PTBebasNeueRegular.ttf";

    ProgressBar mProgressBar;

    AuthPanel mAuthPanel;
Button mLoginBtn,mShowCatalogBtn;
    EditText mEditTextEmail,mEditTextPassw;
CoordinatorLayout mCoordinatorLayout;
  CardView mAuthCard;


    TextView mTextViewCatalog,mTextViewComein,mTextViewAppname;

//region ===========================life cicle=======================================



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);

       mLoginBtn=(Button)findViewById(R.id.login_btn);
        mShowCatalogBtn=(Button)findViewById(R.id.show_catalog_btn);
       mCoordinatorLayout=(CoordinatorLayout)findViewById(R.id.coordinator_container);
        mAuthCard=(CardView)findViewById(R.id.auth_card);
       mAuthPanel=(AuthPanel)findViewById(R.id.auth_wrapper);

        mTextViewCatalog=(TextView)findViewById(R.id.show_catalog_btn);
        mTextViewComein=(TextView)findViewById(R.id.login_btn);
        mTextViewAppname=(TextView)findViewById(R.id.app_name_txt);
        mProgressBar=(ProgressBar)findViewById(R.id.progressBar);

        mEditTextEmail=(EditText)findViewById((R.id.login_email_et));
        mEditTextPassw=(EditText)findViewById((R.id.login_password_et));

        //Set fonts for text from assets
        Typeface fontBook=Typeface.createFromAsset(getAssets(),fontNeueBook);
        Typeface fontRegular=Typeface.createFromAsset(getAssets(),fontNeueRegular);


        mTextViewAppname.setTypeface(fontRegular);
        mTextViewCatalog.setTypeface(fontBook);
        mTextViewComein.setTypeface(fontBook);

//Inicialize wiew
        mPresenter.takeView(this);
       mPresenter.initView();

       mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);


    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion




    //region==========================IAuthView============================================



    @Override
    public void showMessage(String message) {
      //  Snackbar.make(mCoordinatorLayout,message,Snackbar.LENGTH_LONG).show();
        //!!!
Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    //если отладка,то в трек,если уже приложение,то всплыв сообщение
    public void showError(Throwable e) {
if (BuildConfig.DEBUG){
    showMessage(e.getMessage());
    e.printStackTrace();
}else{
    showMessage("Извините,что-то пошло не так");
    //TODO show error stacktrace
}

    }


    @Override
    public void showLoad() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoad() {
        mProgressBar.setVisibility(View.INVISIBLE);

    }



    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }






    public void showAuthCard(){
        mAuthCard.setVisibility(View.VISIBLE);
    }

    public void hideAuthCard(){
        mAuthCard.setVisibility(View.INVISIBLE);
    }




    @Override
    public void showLoginBtn() {
        mLoginBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoginBtn() {
        mLoginBtn.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showCatalogBtn() {
        mShowCatalogBtn.setVisibility(View.VISIBLE);
    }


    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }
//endregion

    @Override
    public void onBackPressed() {

        if (!mAuthPanel.isIdle()) {
            mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);

        }else
        super.onBackPressed();
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.show_catalog_btn:
                mPresenter.clickOnShowCatalog();
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                //testShowLoginCard();
                break;
        }
    }


    /**
     *Retriewe email from sha pref
     */
    public String getEmail(){

        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        return prefs.getString(loginKey,"em");

    }
//    *Retrieves password from sha pref

    public String getPassword(){
        SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        return prefs.getString(passwordKey,"ps");

    }


//saves login and passw
    public void saveDate(){
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();

        if((mAuthPanel.getUserEmail()!=null)&&(mAuthPanel.getUserPassword()!=null)) {
            editor.putString(loginKey, mAuthPanel.getUserEmail());
            editor.putString(passwordKey, mAuthPanel.getUserPassword());
            editor.apply();
        }
    }

    public boolean isEmailAndPassword(){
       return isEmailValidMethod(mEditTextEmail.getText().toString())&&(isValidPassword(mEditTextPassw.getText().toString()));

    }


    public boolean isEmailValidMethod(final String email) {
        if(Patterns.EMAIL_ADDRESS.matcher(email).matches()){
        }else {
            showMessage(getResources().getString(R.string.fix_email_msg));
        }
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 8) {
            return true;
        }else {

        showMessage(getResources().getString(R.string.check_password));

        return false;
        }
    }

}
