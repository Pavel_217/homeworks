package com.pavelwinter.hometask.ui.custom_views;
import android.graphics.Typeface;
import android.widget.TextView;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.pavelwinter.hometask.R;

/**
 * Created by newuser on 13.11.2016.
 */

public class AuthPanel extends LinearLayout {



    public static final String TAG="AuthPanel";

    //поля ввода логина и пароля видны
    public static final int LOGIN_STATE=2;

    //не видны поля ввода логина и пароля
    public static final int IDLE_STATE=1;

    private int mCustomState=1;

    CardView mAuthCard;

    AuthPanel mAuthPanel;

    Button mLoginBtn,mShowCatalogBtn;
    EditText mEtEmail,mEtPassw;

    public AuthPanel(Context context, AttributeSet attrs) {

        super(context, attrs);
    }

    //TODO validate and state for email input
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mAuthCard=(CardView)findViewById(R.id.auth_card);
        mLoginBtn=(Button)findViewById(R.id.login_btn);
        mShowCatalogBtn=(Button)findViewById(R.id.show_catalog_btn);
        mEtEmail=(EditText) findViewById(R.id.login_email_et);
        mEtPassw=(EditText)findViewById(R.id.login_password_et);
        mAuthPanel=(AuthPanel)findViewById(R.id.auth_wrapper);

        showViewFromState();

    }


    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState=super.onSaveInstanceState();
        SavedState savedState=new SavedState(superState);
        savedState.state=mCustomState;
        return savedState;
    }


    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState=(SavedState)state;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCustomState(savedState.state);
    }



    public void setCustomState(int state) {
        mCustomState=state;
      showViewFromState();
    }


    private  void showLoginState(){
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(GONE);
    }


  private void  showIdleState(){
      mLoginBtn.setVisibility(VISIBLE);
      mShowCatalogBtn.setVisibility(VISIBLE);


      mAuthCard.setVisibility(GONE);
    }

 public void showViewFromState(){
if(mCustomState==LOGIN_STATE){
    showLoginState();
}else {
    showIdleState();
}
    }


    public String getUserEmail(){
return String.valueOf(mEtEmail.getText());
    }

    public String getUserPassword(){
        return String.valueOf(mEtPassw.getText());
    }


public boolean isIdle(){
    return mCustomState==IDLE_STATE;
}



    static class SavedState extends BaseSavedState{

        private int state;

        public static final Parcelable.Creator<SavedState> CREATOR=new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

            public SavedState(Parcelable superState){
            super(superState);
            }

                private SavedState(Parcel in){
                    super(in);
                    state=in.readInt();
                }




        public void writeToParcel(Parcel out, int flags){
       super.writeToParcel(out,flags);
            out.writeInt(state);

        }
        }

    }

