package com.pavelwinter.hometask.mvp.presenters;

import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pavelwinter.hometask.R;
import com.pavelwinter.hometask.mvp.models.AuthModel;
import com.pavelwinter.hometask.mvp.views.IAuthPresenter;
import com.pavelwinter.hometask.mvp.views.IAuthView;
import com.pavelwinter.hometask.ui.custom_views.AuthPanel;

import static android.content.ContentValues.TAG;

/**
 * Created by newuser on 13.11.2016.
 */

public class AuthPresenter implements IAuthPresenter {

    private static AuthPresenter ourInstance=new AuthPresenter();

    private AuthModel mAuthModel;
private  IAuthView mIAuthView;

    private AuthPresenter(){
        mAuthModel=new AuthModel();
    }



    public static AuthPresenter getInstance(){

        return ourInstance;
    }


    @Override
    public void takeView(IAuthView authView) {
        mIAuthView=authView;
    }


    @Override
    public void dropView() {
     mIAuthView=null;
    }

    @Override
    public void initView() {


       getView().hideLoad();



        if (getView()!=null){
            Log.i(TAG, "initView:GET VIEW NOT NULL");

     if(checkUserAuth()){
    getView().hideLoginBtn();


         mAuthModel.getEmail(getView().getEmail());
         mAuthModel.getPassword(getView().getPassword());



    }else {
         getView().showLoginBtn();

     }
        }
    }



    @Nullable
    @Override
    public IAuthView getView() {
        return mIAuthView;
    }




    @Override
    public void clickOnLogin() {

if(getView().getAuthPanel()==null) {
    Log.d("TAG","PANEL IS NULL");
}else {    Log.d("TAG","PANEL IS NO NULL");
}


if(getView()!=null&&getView().getAuthPanel()!=null){

            if(getView().getAuthPanel().isIdle()){
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);

            }else mAuthModel.loginUser(getView().getAuthPanel().getUserEmail(),getView().getAuthPanel().getUserPassword());





    //если логин и пароль подошли,запомнить их
    if(getView().isEmailAndPassword()&&checkUserAuth()){

        getView().saveDate();
    }



    getView().showLoad();

    getView().showMessage("Загрузка");

        }    }

    @Override
    public void clickOnFb() {

    }

    @Override
    public void clickOnVk() {

    }

    @Override
    public void clickOnTweet() {

    }

    @Override
    public void clickOnShowCatalog() {

    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }
}