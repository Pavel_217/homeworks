package com.pavelwinter.hometask.mvp.views;

import android.provider.Settings;
import android.support.annotation.Nullable;

import com.pavelwinter.hometask.ui.custom_views.AuthPanel;

/**
 * Created by newuser on 13.11.2016.
 */

public interface IAuthView {

    void showMessage(String message);

    void showError(Throwable e);

    void showLoad();

    void hideLoad();


    IAuthPresenter getPresenter();

    void showLoginBtn();


    void hideLoginBtn();



    void showCatalogBtn();


    @Nullable
   AuthPanel getAuthPanel();

    //void testShowLogin();



    String getEmail();
    String getPassword();
    void saveDate();


    boolean isEmailAndPassword();

    void showAuthCard();

    void hideAuthCard();


  // default void hideAuthCard(){
    //    System.err.print("a");
    //};



}