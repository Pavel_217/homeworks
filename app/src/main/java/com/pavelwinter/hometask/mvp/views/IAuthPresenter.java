package com.pavelwinter.hometask.mvp.views;

import android.support.annotation.Nullable;

/**
 * Created by newuser on 13.11.2016.
 */

public interface IAuthPresenter {

    void takeView(IAuthView authView);
    void dropView();
    void initView();


    @Nullable
    IAuthView getView();


    void clickOnLogin();
    void clickOnFb();
    void clickOnVk();
    void clickOnTweet();
    //catalI
    void clickOnShowCatalog();

    boolean checkUserAuth();

}
